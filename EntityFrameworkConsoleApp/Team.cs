﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkConsoleApp
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sport { get; set; }
        public string League { get; set; }
        public string Nationality { get; set; }
        public int? CoachId { get; set; }
        public Coach Coach { get; set; }
        public ICollection<Competitor> Competitors { get; set; }
    }
}
