﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntityFrameworkConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sport Competition Manager:");
            Console.WriteLine("---------------------------");
            /*AddPlayerToDB();
            DisplayAllPlayersInDB();
            DisplayFirstPlayerInDB();
            FindPlayerBasedOnName();*/

            //AddDataToDB();

            //AddPositionToPlayer();

            /*using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                Competitor competitor = dbContext.Competitors.Where(s => s.Id == 6).Single();
                Team team = dbContext.Teams.Where(s => s.Id == competitor.TeamId).Single();

                Console.WriteLine(team.Name);
            }*/
            AddDataToDB();
        }

        

        public static void DisplayAllPlayersInDB()
        {
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                List<Competitor> allCompetitors = dbContext.Competitors
                                                   .ToList();

                foreach (Competitor competitor in allCompetitors)
                {
                    Console.WriteLine("First name: " + competitor.FirstName + " | Last name: " + competitor.LastName +
                                      " | Age: " + competitor.Age + " | Height: " + competitor.Height +
                                      " | Team: " + competitor.Team + " | " + competitor.Sport);
                }
            }
        }

        public static void DisplayOnePlayerInDB(string name)
        {

        }

        public static void AddPositionToPlayer()
        {
            Position amr = new Position
            {
                Name = "AMR"
            };
            Position amc = new Position
            {
                Name = "AMC"
            };
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                Competitor competitor = dbContext.Competitors.Where(c => c.Id == 6).Single();

                Console.WriteLine(competitor.FirstName);
                dbContext.Positions.Add(amc);
                dbContext.Positions.Add(amr);
                /*CompetitorPosition compos = new CompetitorPosition
                {
                    CompetitorId = 6,
                    PositionId = 1
                };

                dbContext.CompetitorPositions.Add(compos);*/

                /*competitor.CompetitorPositions.Add(new CompetitorPosition
                {
                    Competitor = competitor,
                    Position = amc
                });*/

                //dbContext.SaveChanges();
                var competitorSkill = dbContext.CompetitorPositions.Where(cp => cp.CompetitorId == 6).ToList();
            }
        }

        public static void DisplayFirstPlayerInDB()
        {
            Console.WriteLine("\nFirst Player in DB");
            Console.WriteLine("----------------------");
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                Competitor competitor = dbContext.Competitors.First();
                Console.WriteLine($"{competitor.FirstName} {competitor.LastName} {competitor.Age} {competitor.Height} {competitor.Team} {competitor.Sport}");
            }
        }

        public static void FindPlayerBasedOnName()
        {
            string firstName;
            string lastName;
            Console.WriteLine("------------------------");
            Console.WriteLine("First name: ");
            firstName = Console.ReadLine();
            Console.WriteLine("Last name: ");
            lastName = Console.ReadLine();

            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                Competitor competitor = dbContext.Competitors.Where(c => (c.FirstName == firstName && c.LastName == lastName)).Single();

                Console.WriteLine($"{competitor.FirstName} {competitor.LastName} {competitor.Age} {competitor.Height} {competitor.Team} {competitor.Sport}");
            }
        }
    }
}
