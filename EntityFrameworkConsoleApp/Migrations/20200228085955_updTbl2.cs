﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkConsoleApp.Migrations
{
    public partial class updTbl2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Abbrevation",
                table: "Positions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Abbrevation",
                table: "Positions");
        }
    }
}
