﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkConsoleApp.Migrations
{
    public partial class updCompPos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorPosition_Competitors_CompetitorId",
                table: "CompetitorPosition");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorPosition_Positions_PositionId",
                table: "CompetitorPosition");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompetitorPosition",
                table: "CompetitorPosition");

            migrationBuilder.RenameTable(
                name: "CompetitorPosition",
                newName: "CompetitorPositions");

            migrationBuilder.RenameIndex(
                name: "IX_CompetitorPosition_PositionId",
                table: "CompetitorPositions",
                newName: "IX_CompetitorPositions_PositionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompetitorPositions",
                table: "CompetitorPositions",
                columns: new[] { "CompetitorId", "PositionId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorPositions_Competitors_CompetitorId",
                table: "CompetitorPositions",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorPositions_Positions_PositionId",
                table: "CompetitorPositions",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorPositions_Competitors_CompetitorId",
                table: "CompetitorPositions");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorPositions_Positions_PositionId",
                table: "CompetitorPositions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompetitorPositions",
                table: "CompetitorPositions");

            migrationBuilder.RenameTable(
                name: "CompetitorPositions",
                newName: "CompetitorPosition");

            migrationBuilder.RenameIndex(
                name: "IX_CompetitorPositions_PositionId",
                table: "CompetitorPosition",
                newName: "IX_CompetitorPosition_PositionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompetitorPosition",
                table: "CompetitorPosition",
                columns: new[] { "CompetitorId", "PositionId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorPosition_Competitors_CompetitorId",
                table: "CompetitorPosition",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorPosition_Positions_PositionId",
                table: "CompetitorPosition",
                column: "PositionId",
                principalTable: "Positions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
