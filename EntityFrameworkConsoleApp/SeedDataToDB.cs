﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntityFrameworkConsoleApp
{
    public class SeedDataToDB
    {
        public SeedDataToDB()
        {
            AddCoachesToDB();
            AddTeamsToDB();
            AddPositionsToDB();
            AddPlayersToDB();
            AddPositionsToCompetitors();
        }

        public static void AddCoachesToDB()
        {
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                Coach ole = new Coach
                {
                    FirstName = "Ole Gunnar",
                    LastName = "Solskjær",
                    Age = 47
                };

                dbContext.Coaches.Add(ole);

                dbContext.SaveChanges();
            }
        }

        public static void AddTeamsToDB()
        {
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                Team team1 = new Team
                {
                    Name = "Manchester United",
                    Sport = "Football",
                    Coach = dbContext.Coaches.Where(coach => coach.FirstName == "Ole Gunnar").Single()
                };

                dbContext.Teams.Add(team1);

                dbContext.SaveChanges();
            }
        }

        public static void AddPositionsToDB()
        {
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {

                dbContext.Positions.Add(new Position
                {
                    Name = "Goalkeeper",
                    Abbrevation = "GK"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Defender Right",
                    Abbrevation = "DR"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Defender Left",
                    Abbrevation = "DL"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Central Defender",
                    Abbrevation = "CD"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Defensive Midfielder",
                    Abbrevation = "DM"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Central Midfielder",
                    Abbrevation = "CM"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Right Midfielder",
                    Abbrevation = "RM"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Left Midfielder",
                    Abbrevation = "LM"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Attacking Midfielder",
                    Abbrevation = "AM"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Right Winger",
                    Abbrevation = "RW"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Left Winger",
                    Abbrevation = "LW"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Striker",
                    Abbrevation = "ST"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Right Wing Back",
                    Abbrevation = "RWB"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Left Wing Back",
                    Abbrevation = "LWB"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Attacking Midfielder Right",
                    Abbrevation = "AMR"
                });

                dbContext.Positions.Add(new Position
                {
                    Name = "Attacking Midfielder Left",
                    Abbrevation = "AML"
                });

                dbContext.SaveChanges();
            }
        }

        public static void AddPlayersToDB()
        {
            Competitor competitor;
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "David",
                    LastName = "de Gea",
                    Age = 29,
                    Nationality = "Spain",
                    Height = 192,
                    Team = dbContext.Teams.Where(team => team.Id == 1).Single()
                });

                 dbContext.Competitors.Add(new Competitor
                 {
                     FirstName = "Aaron",
                     LastName = "wan Bissaka",
                     Age = 22,
                     Height = 183,
                     Nationality = "England",
                     Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                 });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Luke",
                    LastName = "Shaw",
                    Age = 24,
                    Height = 185,
                    Nationality = "England",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Harry",
                    LastName = "Maguire",
                    Age = 26,
                    Height = 194,
                    Nationality = "England",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Victor",
                    LastName = "Lindelöf",
                    Age = 25,
                    Height = 187,
                    Nationality = "Sweden",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Fred",
                    LastName = "Santos",
                    Age = 26,
                    Height = 169,
                    Nationality = "Brazil",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Scott",
                    LastName = "McTominay",
                    Age = 23,
                    Height = 193,
                    Nationality = "England",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Bruno",
                    LastName = "Fernandes",
                    Age = 25,
                    Height = 179,
                    Nationality = "Portugal",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Marcus",
                    LastName = "Rashford",
                    Age = 22,
                    Height = 180,
                    Nationality = "England",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Mason",
                    LastName = "Greenwood",
                    Age = 18,
                    Height = 180,
                    Nationality = "England",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.Competitors.Add(new Competitor
                {
                    FirstName = "Anthony",
                    LastName = "Martial",
                    Age = 24,
                    Height = 181,
                    Nationality = "France",
                    Team = dbContext.Teams.Where(team => team.Name == "Manchester United").Single()
                });

                dbContext.SaveChanges();
            }
        }

        public static void AddPositionsToCompetitors()
        {
            using (SportCompetitionDBContext dbContext = new SportCompetitionDBContext())
            {
                var competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "David" && comp.LastName == "de Gea")).Single();
                Console.WriteLine(competitor.Id);
                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 1
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Aaron" && comp.LastName == "wan Bissaka")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 2
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 13
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Luke" && comp.LastName == "Shaw")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 3
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 14
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Harry" && comp.LastName == "Maguire")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 4
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Victor" && comp.LastName == "Lindelöf")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 4
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Fred" && comp.LastName == "Santos")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 5
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 6
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Scott" && comp.LastName == "McTominay")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 5
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 6
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Bruno" && comp.LastName == "Fernandes")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 6
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 9
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 15
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 16
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Marcus" && comp.LastName == "Rashford")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 12
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 11
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Anthony" && comp.LastName == "Martial")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 12
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 11
                });

                competitor = dbContext.Competitors.Where(comp => (comp.FirstName == "Mason" && comp.LastName == "Greenwood")).Single();

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 12
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 11
                });

                dbContext.CompetitorPositions.Add(new CompetitorPosition
                {
                    CompetitorId = competitor.Id,
                    PositionId = 9
                });

                dbContext.SaveChanges();
            }
        }
    }
}
