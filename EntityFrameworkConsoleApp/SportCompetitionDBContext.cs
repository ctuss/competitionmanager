﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkConsoleApp
{
    public class SportCompetitionDBContext : DbContext
    {
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<CompetitorPosition> CompetitorPositions { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"server=PC7373\SQLEXPRESS;Database=SportCompetition;trusted_connection=true;MultipleActiveResultSets=true;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompetitorPosition>().HasKey(pq => new { pq.CompetitorId, pq.PositionId });
            SeedDataToDB seed = new SeedDataToDB();
        }
    }   
}