﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkConsoleApp.Migrations
{
    public partial class crt3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "League",
                table: "Teams",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "Teams",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "League",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "Teams");
        }
    }
}
