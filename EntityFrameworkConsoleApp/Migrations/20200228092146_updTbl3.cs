﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkConsoleApp.Migrations
{
    public partial class updTbl3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "Competitors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "Coaches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "Coaches");
        }
    }
}
