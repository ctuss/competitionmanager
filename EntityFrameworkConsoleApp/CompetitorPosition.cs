﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkConsoleApp
{
    public class CompetitorPosition
    {
        public int CompetitorId { get; set; }
        public Competitor Competitor { get; set; }
        public int PositionId { get; set; }
        public Position Position { get; set; } 
    }
}
