﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFrameworkConsoleApp
{
    public class Competitor : Person
    {   
        public int Height { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public string Sport { get; set; }
        public ICollection<CompetitorPosition> CompetitorPositions { get; set; }
    }
}
