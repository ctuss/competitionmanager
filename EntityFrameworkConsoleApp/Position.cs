﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkConsoleApp
{
    public class Position
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbrevation { get; set; }
        public ICollection<CompetitorPosition> CompetitorPositions { get; set; }
    }
}
